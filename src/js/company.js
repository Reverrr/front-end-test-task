const list_companies = $('#list-companies'),
    companyPartnersName = $('#company-partners-name'),
    companyPartnersPercentage = $('#company-partners-percentage'),
    sortByCompanyPartnersName = $('#sort-by-company-partners-name'),
    sortByCompanyPartnersPercentage = $('#sort-by-company-partners-percentage'),
    listLocationCompanyChart = $('#companies-location-chart'),
    news = $('#news-list'),
    backToChart = $('#back-to-chart').hide(),
    content = $('.content').hide(),
    listOfSelectedCountryCompanies = $('#list-of-selected-country-companies').hide(),
    loader = $('.company-loader');

let active_list_company;
let listPartnersData = [];

let currentObjSort = {
    name: 'value',
    sortByASC: false
};

let mapCountryData = new Map();

const getListCompany = new Promise((resolve, reject) => {
    $.get( 'http://codeit.pro/codeitCandidates/serverFrontendTest/company/getList', function(dataFromServer) {
        if (dataFromServer.status === 'OK') {
            resolve(dataFromServer.list);
        } else {
            reject(`Status: ${dataFromServer.status}`);
        }
    });
});
getListCompany.then(
    (data) => {
        content.show();
        loader.hide();

        $('#total-company-count').html(`<span>${data.length}</span>`);
        data.forEach((company, i) => {
            /**
             Create data for listCompanyChart value
             */
            const getCodeCountry = mapCountryData.get(company.location.code);
            if (!getCodeCountry) {
                mapCountryData.set(company.location.code, {code: company.location.code, nameCompany: [company.name], count: 1});
            } else {
                getCodeCountry.nameCompany.push(company.name);
                getCodeCountry.count += 1
            }
            /**
             Create list company
             */
            list_companies.append(`<li><a href="#" id="company_${i}">${company.name}</a></li>`);
        });
        list_companies.on('click', (event) => {
            event.preventDefault();
            const indexClickedElement = data.findIndex((elem) => elem.name === event.target.innerHTML);
            if (!!active_list_company) {
                active_list_company.removeClass('active');
            }
            active_list_company = $(`#company_${indexClickedElement}`);
            active_list_company.attr('class', 'active');
            listPartnersData = [];
            data[indexClickedElement].partners.forEach((partner) => { /* clone partners */
                listPartnersData.push(partner);
            });
            templateCompanyPartners();
        });
        /**
         create Sort
         */
        sortByCompanyPartnersName.on('click', (event) => {
            event.preventDefault();
            currentObjSort.name = 'name';
            currentObjSort.sortByASC = !currentObjSort.sortByASC;
            templateCompanyPartners();
        });
        sortByCompanyPartnersPercentage.on('click', (event) => {
            event.preventDefault();
            currentObjSort.name = 'value';
            currentObjSort.sortByASC = !currentObjSort.sortByASC;
            templateCompanyPartners();
        });
        /**
        create Chart
        */
        let labelForChart = [];
        let percentForChart = [];
        mapCountryData.forEach((item) => {
            labelForChart.push(item.code);
            percentForChart.push(item.count*100/data.length);
        });

        if (!!listLocationCompanyChart[0]) {
            const listCompanyChart = new Chart(listLocationCompanyChart, {
                type: 'pie',
                data: {
                    labels: labelForChart,
                    datasets: [{
                        data: percentForChart,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    legend: {
                        display: false
                    }
                }
            });
            listLocationCompanyChart.on('click', function (event) {
                let firstPoint = listCompanyChart.getElementAtEvent(event)[0];
                if (firstPoint) {
                    let label = listCompanyChart.data.labels[firstPoint._index];
                    listLocationCompanyChart.hide();
                    listOfSelectedCountryCompanies.html('');
                    mapCountryData.get(label).nameCompany.forEach((company) => {
                        listOfSelectedCountryCompanies.append(`<li>${company}</li>`);
                    });
                    backToChart.show();
                    listOfSelectedCountryCompanies.show();
                }
            });
            backToChart.on('click', () => {
                listLocationCompanyChart.show();
                backToChart.hide();
                listOfSelectedCountryCompanies.hide();
            });
        }
    },
    (error) => {
        new Error(`Failed to get data: ${error}`);
    }
);

const getListNews = new Promise((resolve, reject) => {
    $.get('http://codeit.pro/codeitCandidates/serverFrontendTest/news/getList')
        .done((data) => {
            if (data.status === 'OK') {
                resolve(data.list);
            } else {
                reject(`news status: ${news.status}`);
            }
        });
});
getListNews.then(
    (data) => {
        data.forEach((item) => {
            const date = new Date(item.date*1000);
            news.append(`
            <div class="news">
                <div class="news-info">
                    <img src="${item.img}" alt="img">
                    <div>Author: ${item.author}</div>
                    <div>Public: ${
                        (date.getDate() < 10) ? '0'+date.getDate() : date.getDate()
                        }.${(date.getMonth() < 10) ? '0'+date.getMonth() : date.getMonth()
                        }.${date.getFullYear()
                        }
                    </div>
                </div>
                <div class="news-text">
                    <h2><a href= "//${item.link}">Coming soon</a></h2>
                    <p>${item.description}</p>
                </div>
            </div>`);
        });
        /**
         * Clone first element
         * */
        $('#news-list').append($('#news-list .news').first().clone());
        let currentNewsList = 1;
        let newsListRight = 0;
        /**
         * News carousel
         * */
        setInterval(() => {
            if (newsListRight > 70 * data.length + 1) {
                currentNewsList = 1;
                newsListRight = 0;
                news.css('right', '0ch');
            }
            if (data.length !== currentNewsList) {
                news.animate({'right': `${newsListRight += 70}ch`});
                currentNewsList += 1;
            } else if (data.length === currentNewsList) {
                news.animate({'right': `${newsListRight += 70}ch`}, {
                    complete: () => {
                        currentNewsList = 1;
                        newsListRight = 0;
                        news.css('right', '0ch');
                    }
                });
            } else {
                currentNewsList = 1;
                newsListRight = 0;
                news.css('right', '0ch');
            }
        }, 7000);
    },
    (error) => {
        new Error(`Failed to get data: ${error}`);
    }
);

Chart.plugins.register({
    afterDatasetsDraw: function(chartInstance) {
        let ctx = chartInstance.chart.ctx;
        chartInstance.data.datasets.forEach(function(dataset, i) {
            let meta = chartInstance.getDatasetMeta(i);
            if (!meta.hidden) {
                meta.data.forEach(function(element, index) {
                    ctx.fillStyle = '#000';
                    let fontSize = 12;
                    let fontStyle = 'normal';
                    let fontFamily = 'Helvetica Neue';
                    let media1800px = window.matchMedia("(min-width: 1800px)");
                    (media1800px.matches) ? fontSize = 40 : fontSize = 12;
                    ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
                    let dataString = dataset.data[index].toString();
                    ctx.textAlign = 'center';
                    ctx.textBaseline = 'middle';
                    let position = element.tooltipPosition();
                    ctx.fillText(dataString + '%', position.x, position.y);
                });
            }
        });
    }
});

function templateCompanyPartners() {
    companyPartnersName.html('');
    companyPartnersPercentage.html('');
    listPartnersData = sortCompanyPartners(listPartnersData, currentObjSort.name, currentObjSort.sortByASC);
    listPartnersData.forEach((partner) => {
        companyPartnersName.append(`<div>${partner.name}</div>`);
        companyPartnersPercentage.append(`<div>${partner.value}%</div>`);
    });
}
/**
 * sort = true it is sort by ASC
 * sort = false it is sort by DESC
 * */
function sortCompanyPartners(arr, sortByName, sort = true) {
    if (sort) {
        return arr.sort(function compare(a,b) {
            if (a[sortByName] < b[sortByName])
                return -1;
            if (a[sortByName] > b[sortByName])
                return 1;
            return 0;
        });
    } else {
        return arr.sort(function compare(a,b) {
            if (a[sortByName] > b[sortByName])
                return -1;
            if (a[sortByName] < b[sortByName])
                return 1;
            return 0;
        });
    }
}