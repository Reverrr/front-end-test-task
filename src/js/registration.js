const form_registration = $('#form_registration'),
      form_name = $('#form_registration #name'),
      form_secondname = $('#form_registration #secondname'),
      form_email = $('#form_registration #email'),
      form_gender = $('#form_registration #gender'),
      form_pass = $('#form_registration #pass'),
      form_checkbox = $('#form_registration #checkbox'),
      error_name = $('#form_registration #error-name').hide(),
      error_secondname = $('#form_registration #error-secondname').hide(),
      error_email = $('#form_registration #error-email').hide(),
      error_gender = $('#form_registration #error-gender').hide(),
      error_pass = $('#form_registration #error-pass').hide(),
      error_checkbox = $('#form_registration #error-checkbox').hide(),
    error_submit = $('#form_registration #error-submit').hide();

let formElements = [form_name, form_secondname, form_email, form_gender, form_pass, form_checkbox,];

form_registration.submit(function( event ) {
    let dataJSON = $(this).serializeFormJSON();
        const validationCheckError = formElements.filter((element) => {
        return element.isCorrect === false;
    });
    if (!validationCheckError.length) {
        $.post( 'http://codeit.pro/codeitCandidates/serverFrontendTest/user/registration', dataJSON )
            .done(function( data ) {
                error_submit.hide();
                if (data.status !== 'OK') {
                    switch (data.field) {
                        case 'name': {
                            return error_submit.html(`${data.message}`).show();
                        }
                        case 'secondname': {
                            return error_submit.html(`${data.message}`).show();
                        }
                        case 'email': {
                            return error_submit.html(`${data.message}`).show();
                        }
                        case 'gender': {
                            return error_submit.html(`${data.message}`).show();
                        }
                        case 'pass': {
                            return error_submit.html(`${data.message}`).show();
                        }
                        case 'checkbox': {
                            return error_submit.html(`${data.message}`).show();
                        }
                    }
                } else {
                    error_submit.hide();
                    window.location.replace('./company.html');
                }
            })
            .fail(function() {
                error_submit.show();
            });
        }
    event.preventDefault();
});

eventErrorOutput(form_name, error_name, /^[a-z]+$/i);
eventErrorOutput(form_secondname, error_secondname, /^[a-z]+$/i);
eventErrorOutput(form_email, error_email, /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);

form_gender.on('keyup', () => {
    if (form_gender.hasClass('untouched')) {
        if (form_gender.val() !== 'empty') {
            form_gender.isCorrect = true;
            form_gender.removeClass('error-value');
            error_gender.hide();
        } else {
            form_gender.isCorrect = false;
            form_gender.addClass('error-value');
            error_gender.show();
        }
    }
});
form_gender.on('focusout', () => {
    if (!form_gender.hasClass('untouched')) {
        if (form_gender.val() !== 'empty') {
            form_gender.addClass('untouched');
            form_gender.isCorrect = true;
            form_gender.removeClass('error-value');
            error_gender.hide();
        } else {
            form_gender.isCorrect = false;
            form_gender.addClass('error-value');
            error_gender.show();
        }
    }
});

form_pass.on('keyup', () => {
    if (form_pass.hasClass('untouched')) {
        if (form_pass.val().length < 4
            || form_pass.val().length > 16) {
            form_pass.isCorrect = false;
            form_pass.addClass('error-value');
            error_pass.show();
        } else {
            form_pass.isCorrect = true;
            form_pass.removeClass('error-value');
            error_pass.hide();
        }
    }
});
form_pass.on('focusout', () => {
    if (!form_pass.hasClass('untouched')) {
        if (form_pass.val().length < 4
            || form_pass.val().length > 16) {
            form_pass.addClass('untouched');
            form_pass.isCorrect = false;
            form_pass.addClass('error-value');
            error_pass.show();
        } else {
            form_pass.isCorrect = true;
            form_pass.removeClass('error-value');
            error_pass.hide();
        }
    }
});

form_checkbox.on('click', () => {
    if (!form_checkbox.prop('checked')) {
        error_checkbox.show();
        form_checkbox.isCorrect = false;
        form_checkbox.addClass('error-value');
    } else {
        form_checkbox.isCorrect = true;
        form_checkbox.removeClass('error-value');
        error_checkbox.hide();
    }
});

/**
 * Function which check input on did you enter data
 * */
function eventErrorOutput(element_form, error_name,  reg = '') {
    element_form.on('keyup', () => {
        if (element_form.hasClass('untouched')) {
            validationOfData(element_form, error_name, reg);
        }
    });
    element_form.on('focusout', () => {
        if (!element_form.hasClass('untouched')) {
            element_form.addClass('untouched');
            validationOfData(element_form, error_name, reg);
        }
    });
}

/**
 * Function which check input on valid data
 * */
function validationOfData(element, error, reg = '') {
    if (element.val()
        && typeof element.val() === 'string'
        && element.val().length > 2
        && element.val().match(reg)
    ) {
        element.isCorrect = true;
        element.removeClass('error-value');
        error.hide();
    } else {
        element.isCorrect = false;
        console.log(element);
        element.addClass('error-value');
        error.show();
    }
}

/**
 * Create method to serialize form to JSON
 * */
jQuery.fn.extend({
    serializeFormJSON: function () {
        let obj = {};
        let array = this.serializeArray();
        $.each(array, function () {
            if (obj[this.name]) {
                if (!obj[this.name].push) {
                    obj[this.name] = [obj[this.name]];
                }
                obj[this.name].push(this.value || '');
            } else {
                obj[this.name] = this.value || '';
            }
        });
        return obj;
    }
});
