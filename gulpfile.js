const gulp = require('gulp'),
      babel = require('gulp-babel'),
      concat = require('gulp-concat'),
      sass = require('gulp-sass'),
      uglify = require('gulp-uglify');

sass.compiler = require('node-sass');

gulp.task('concat_js_lib', function() {
    return gulp.src(['src/lib/*.js'])
        .pipe(concat('libs.js'))
        .pipe(gulp.dest('dist'));
});

gulp.task('concat_css_lib', function() {
    return gulp.src(['src/lib/*.css'])
        .pipe(concat('libs.css'))
        .pipe(gulp.dest('dist'));
});

gulp.task('scripts', function() {
    return gulp.src(['src/js/*.js'])
        .pipe(babel({
            presets: ['@babel/env'],
            plugins: ['@babel/transform-runtime']
        }))
        .pipe(concat('all-min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist'));
});

gulp.task('img', function() {
    return gulp.src(['src/img/*'])
        .pipe(gulp.dest('dist'));
});

gulp.task('pages', function() {
    return gulp.src(['*.html'])
        .pipe(gulp.dest('dist'));
});

gulp.task('sass', function () {
    return gulp.src('src/sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('all.css'))
        .pipe(gulp.dest('dist'));
});

gulp.task('watch', function () {
    gulp.watch(['src/js/*.js', 'src/sass/*.scss', '*.html', 'src/img/*'], gulp.series('scripts', 'sass', 'pages', 'img'))
});

gulp.task('build', gulp.series('concat_js_lib', 'concat_css_lib', 'scripts', 'sass', 'img', 'pages'));

gulp.task('default', gulp.series('concat_js_lib', 'concat_css_lib', 'scripts', 'sass', 'img', 'pages', 'watch'));
